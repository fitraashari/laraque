<?php

namespace App\Listeners;

use App\Events\BlogPublishEvent;
use App\Mail\BlogPublish;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailBlogPublishAuthor implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  BlogPublishEvent  $event
     * @return void
     */
    public function handle(BlogPublishEvent $event)
    {
        //kirim email ke pembuat blog bahwa blognya sudah di publis
        Mail::to($event->blog->user)->send(new BlogPublish($event->blog));
    }
}

<?php

namespace App\Listeners;

use App\Events\NewBlogEvent;
use App\Mail\NewEditorBlog;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailNewBlogEditor implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewBlogEvent  $event
     * @return void
     */
    public function handle(NewBlogEvent $event)
    {   
        //kirim email ke semua user yang statusnya sebagai editor bahwa ada blog baru yang harus di review
        foreach($event->editors as $editor){
            // dd($editor);
            Mail::to($editor)->send(new NewEditorBlog($event->blog,$editor));
          }
    }
}

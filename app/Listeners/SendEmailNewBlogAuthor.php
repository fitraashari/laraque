<?php

namespace App\Listeners;

use App\Events\NewBlogEvent;
use App\Mail\NewAuthorBlog;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Support\Facades\Mail;

class SendEmailNewBlogAuthor implements ShouldQueue
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewBlogEvent  $event
     * @return void
     */
    public function handle(NewBlogEvent $event)
    {   
        //kirim email ke pembuat blog bahwa blognya sudah di buat
        Mail::to($event->blog->user)->send(new NewAuthorBlog($event->blog));
    }
}

<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Str;
use App\Traits\UsesUuid;
class Blog extends Model
{
    //
    use UsesUuid;

    protected $guarded=[];
    public function user(){
        return $this->belongsTo(User::class);
    }
}

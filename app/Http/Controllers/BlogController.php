<?php

namespace App\Http\Controllers;

use App\Blog;
use App\Events\BlogPublishEvent;
use App\Events\NewBlogEvent;
use App\User;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        if(User::isEditor()){
            $blog = Blog::get();
        }else{
            $blog = auth()->user()->blog;
        }
        return response()->json([
            'data'=>$blog
        ],200);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
         $editors = $this->getEditor();
        //  $editors_email=[];
        //  foreach($editors as $editor){
        //      $editors_email[]=$editor->email;
        //  }
        // return $editors_email;
          request()->validate([
              'title'=>['required'],
              'content'=>['required']
          ]);
          $blog = Blog::create([
              'title'=>request('title'),
              'content'=>request('content'),
              'slug'=> \Str::slug(request('title')),
              'user_id'=>auth()->user()->id
          ]);
          //jika user bukan editor maka kirim email notifikasi
            if(!User::isEditor()){
                event(new NewBlogEvent($blog,$editors));
            }

          return response()->json([
              'message'=>'Berhasil Membuat Blog',
              'data'=>$blog
          ],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Blog $blog)
    {
        //
        return response()->json([
            'data'=>$blog
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Blog $blog)
    {
        request()->validate([
            'title'=>['required'],
            'content'=>['required']
        ]);
        if(User::isEditor()||$blog->user->id==auth()->user()->id){
        $blog->update([
            'title'=>request('title'),
            'content'=>request('content')
        ]);
        }else{
            return response()->json([
                'message'=>'Denied'
            ],403);
        }
        return response()->json([
            'message'=>'Berhasil Mengupdate Blog',
            'data'=>$blog
        ],200);
              
    }
    public function changePublish(Blog $blog){
        if($blog->publish_status==1){
            $status = 0;
            $msg="Blog Berhasil Unpublish";
        }else{
            $status = 1;
            $msg="Blog Berhasil Di Publish";
            event(new BlogPublishEvent($blog));
        }
        $blog->update([
            'publish_status'=>$status,
        ]);

        return response()->json([
            'message'=>$msg,
            'data'=>$blog
        ],200);
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Blog $blog)
    {
        //
        if(User::isEditor()||$blog->user->id==auth()->user()->id){
            $blog->delete();
            }else{
                return response()->json([
                    'message'=>'Denied'
                ],403);
            }
        return response()->json([
            'message'=>'Berhasil Menghapus Data',
        ],200);
    }
    public function getEditor(){
        $editor = User::where('role','editor')->get();
        return $editor;
    }
}

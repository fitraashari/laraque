<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\RegisterRequest;
use App\User;
use Illuminate\Http\Request;

class RegisterController extends Controller
{
    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(RegisterRequest $request)
    {
        //
        // return $request['username'];
        $role = !empty($request['role'])?$request['role']:'author';
         $user = User::create([
             'username'=>$request['username'],
             'name'=>$request['name'],
             'email'=>$request['email'],
             'password'=>bcrypt($request['password']),
             'role'=>$role,
         ]);
         return response()->json([
             'message'=>'Success',
             'data'=>$user
         ],200);
    }
}

<?php

namespace App\Mail;

use App\Blog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class BlogPublish extends Mailable
{
    use Queueable, SerializesModels;
    protected $blog;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Blog $blog)
    {
        //
        $this->blog=$blog;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@azureryu.com')
                ->view('send_email_blog_publish')
                ->subject('Selamat! Blog Anda Sudah Di Publish')
                ->with([
                    'name' => $this->blog->user->name,
                    'title'=> $this->blog->title
                    ]);
    }
}

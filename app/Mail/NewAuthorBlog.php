<?php

namespace App\Mail;

use App\Blog;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewAuthorBlog extends Mailable
{
    use Queueable, SerializesModels;

        protected $blog;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Blog $blog)
    {
        //
        $this->blog = $blog;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@azureryu.com')
                ->view('send_email_author_new_blog')
                ->subject('Blog Baru Telah Dibuat')
                ->with([
                    'name' => $this->blog->user->name,
                    'title'=> $this->blog->title
                    ]);
    }
}

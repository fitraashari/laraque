<?php

namespace App\Mail;

use App\Blog;
use App\User;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class NewEditorBlog extends Mailable
{
    use Queueable, SerializesModels;
    protected $editor;
    protected $blog;
    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(Blog $blog,User $editor)
    {
        //
        $this->blog = $blog;
        $this->editor = $editor;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->from('no-reply@azureryu.com')
                ->view('send_email_editor_new_blog')
                ->subject('Ada Blog Baru Nih, Tolong Di Review!')
                ->with([
                    'name' => $this->blog->user->name,
                    'title'=> $this->blog->title,
                    'editor_name'=>$this->editor->name
                    ]);
    }
}

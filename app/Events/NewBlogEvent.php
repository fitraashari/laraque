<?php

namespace App\Events;

use App\Blog;
use App\User;
use Illuminate\Broadcasting\Channel;
use Illuminate\Broadcasting\InteractsWithSockets;
use Illuminate\Broadcasting\PresenceChannel;
use Illuminate\Broadcasting\PrivateChannel;
use Illuminate\Contracts\Broadcasting\ShouldBroadcast;
use Illuminate\Foundation\Events\Dispatchable;
use Illuminate\Queue\SerializesModels;

class NewBlogEvent
{
    use Dispatchable, InteractsWithSockets, SerializesModels;
    public $blog;
    public $editors;
    /**
     * Create a new event instance.
     *
     * @return void
     */
    public function __construct(Blog $blog, $editors)
    {
        //
        $this->blog = $blog;
        $this->editors = $editors;
    }

    /**
     * Get the channels the event should broadcast on.
     *
     * @return \Illuminate\Broadcasting\Channel|array
     */
    // public function broadcastOn()
    // {
    //     return new PrivateChannel('channel-name');
    // }
}

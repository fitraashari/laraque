<?php

// use Illuminate\Http\Request;

// /*
// |--------------------------------------------------------------------------
// | API Routes
// |--------------------------------------------------------------------------
// |
// | Here is where you can register API routes for your application. These
// | routes are loaded by the RouteServiceProvider within a group which
// | is assigned the "api" middleware group. Enjoy building your API!
// |
// */

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('register','Auth\RegisterController');
Route::post('login','Auth\LoginController');

Route::middleware('auth:api')->group(function(){
    Route::post('logout','Auth\LogoutController');
    Route::get('user','UserController');
    Route::get('blog','BlogController@index');
    Route::get('blog/{blog}','BlogController@show');
    Route::post('blog/create','BlogController@store');
    Route::delete('blog/{blog}/delete','BlogController@destroy');
    Route::post('blog/{blog}/publish','BlogController@changePublish')->middleware('role:editor');
    Route::patch('blog/{blog}/update','BlogController@update');
});
